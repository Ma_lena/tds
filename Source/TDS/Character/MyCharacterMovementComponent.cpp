// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS/Character/MyCharacterMovementComponent.h"
#include "MyCharacterMovementComponent.h"
#include "TDS/Character/TDSCharacter.h"

float UMyCharacterMovementComponent::GetMaxSpeed() const
{
	const float MaxSpeed = Super::GetMaxSpeed();
	const ATDSCharacter* Player = Cast<ATDSCharacter>(GetPawnOwner());
	return Player && Player->IsSprint() ? MaxSpeed * SprintModifier : MaxSpeed;
	
}
